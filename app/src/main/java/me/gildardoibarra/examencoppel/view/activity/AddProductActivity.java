package me.gildardoibarra.examencoppel.view.activity;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Function;
import me.gildardoibarra.examencoppel.ExamenCoppelApplication;
import me.gildardoibarra.examencoppel.R;
import me.gildardoibarra.examencoppel.data.model.Product;
import me.gildardoibarra.examencoppel.data.retrofit.ApiRetrofitCallBack;
import me.gildardoibarra.examencoppel.interactor.AddProductInteractor;
import me.gildardoibarra.examencoppel.presenter.AddProductPresenter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class AddProductActivity extends AppCompatActivity implements AddProductPresenter.View {

    AddProductPresenter presenter;

    @BindView(R.id.btnTakeImageProducts) Button btnCapturePicture;
    @BindView(R.id.txtProductName) EditText txtProductName;
    @BindView(R.id.txtProductDescription) EditText txtProductDescription;
    @BindView(R.id.btnAddProduct) Button btnAddProduct;
    @BindView(R.id.imgView) ImageView imgView;
    @BindView(R.id.pb_progress) View viewProgress;
    @BindView(R.id.frmAddProduct) LinearLayout frmAddProduct;
    private Bitmap productImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        ButterKnife.bind(this);

        setTitle(getString(R.string.add_product_title));

        this.presenter = new AddProductPresenter(new AddProductInteractor(this));
        this.presenter.setView(this);

        btnCapturePicture.setOnClickListener((View v) -> startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), 0) );
        btnAddProduct.setOnClickListener((View v) -> this.presenter.addProduct(txtProductName.getText().toString(), txtProductDescription.getText().toString()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            productImage = (Bitmap) extras.get("data");
            imgView.setImageBitmap(productImage);
            btnCapturePicture.setVisibility(View.GONE);
            btnAddProduct.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showProgress(boolean show) {
        this.presenter.showProgress(show, frmAddProduct, viewProgress);
    }

    @Override
    public void uploadImage(String id) {
        this.presenter.uploadImage(productImage, id);
    }

    @Override
    public AppCompatActivity context() {
        return AddProductActivity.this;
    }
}
