package me.gildardoibarra.examencoppel.view.activity;

import me.gildardoibarra.examencoppel.R;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.gildardoibarra.examencoppel.interactor.LoginInteractor;
import me.gildardoibarra.examencoppel.presenter.LoginPresenter;

/**
 * Created by anzendigital on 12/04/18.
 */

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View {

    private LoginPresenter presenter;

    @BindView(R.id.email) EditText txtUsername;
    @BindView(R.id.password) EditText txtPassword;
    @BindView(R.id.pb_progress) View viewProgress;
    @BindView(R.id.login_form) View viewLoginForm;
    @BindView(R.id.email_sign_in_button) Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        presenter = new LoginPresenter(new LoginInteractor(this));
        presenter.setView(this);

        txtPassword.setOnEditorActionListener((TextView textView, int id, KeyEvent keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                submitLogin();
                return true;
            }
            return false;
        });

        btnLogin.setOnClickListener((View view) -> submitLogin());
    }

    @Override
    public AppCompatActivity context() {
        return LoginActivity.this;
    }

    @Override
    public void submitLogin() {
        presenter.submitLogin(this.txtUsername, this.txtPassword);
    }

    @Override
    public void showProgress(boolean show) {
        presenter.showProgress(show, viewLoginForm, viewProgress);
    }

    @Override
    public FragmentManager getTheFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void onBackPressed() {
        return;
    }
}

