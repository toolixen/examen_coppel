package me.gildardoibarra.examencoppel.view.activity;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.gildardoibarra.examencoppel.R;
import me.gildardoibarra.examencoppel.data.model.Product;
import me.gildardoibarra.examencoppel.interactor.ListProductInteractor;
import me.gildardoibarra.examencoppel.presenter.ListProductPresenter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class ListProductActivity extends AppCompatActivity implements ListProductPresenter.View {

    ListProductPresenter presenter;

    @BindView(R.id.lstProduct) RecyclerView lstProduct;
    @BindView(R.id.login_progress) View viewProgress;

    private ArrayList<Product> datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_products);
        ButterKnife.bind(this);

        setTitle(getString(R.string.list_product_title));

        this.presenter = new ListProductPresenter(new ListProductInteractor(this));
        this.presenter.setView(this);

        this.presenter.loadProducts();
    }

    @Override
    public void showProgress(boolean show) {
        this.presenter.showProgress(show, lstProduct, viewProgress);
    }

    @Override
    public void fillProductList(ArrayList<Product> datos) {
        this.presenter.fillProductList(lstProduct, datos);
    }

    @Override
    public AppCompatActivity context() {
        return ListProductActivity.this;
    }
}
