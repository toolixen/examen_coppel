package me.gildardoibarra.examencoppel.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import me.gildardoibarra.examencoppel.R;
import me.gildardoibarra.examencoppel.data.model.Product;

/**
 * Created by anzendigital on 15/04/18.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> implements View.OnClickListener {

    private ArrayList<Product> datos;
    private View.OnClickListener listener;

    public ProductAdapter(ArrayList<Product> datos) {
        this.datos = datos;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_product, parent, false);

        itemView.setOnClickListener(this);

        ProductViewHolder tvh = new ProductViewHolder(itemView);

        return tvh;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Product item = datos.get(position);
        holder.bindProduct(item);
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder {
        private TextView lblName;
        private TextView lblDescription;

        public ProductViewHolder(View itemView) {
            super(itemView);

            lblName = itemView.findViewById(R.id.lblName);
            lblDescription = itemView.findViewById(R.id.lblDescription);
        }

        public void bindProduct(Product t) {
            lblName.setText(t.getName());
            lblDescription.setText(t.getDescription());
        }
    }
}
