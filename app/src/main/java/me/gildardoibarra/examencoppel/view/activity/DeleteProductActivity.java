package me.gildardoibarra.examencoppel.view.activity;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.gildardoibarra.examencoppel.R;
import me.gildardoibarra.examencoppel.data.model.Product;
import me.gildardoibarra.examencoppel.interactor.DeleteProductInteractor;
import me.gildardoibarra.examencoppel.presenter.DeleteProductPresenter;
import me.gildardoibarra.examencoppel.presenter.LoginPresenter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;

public class DeleteProductActivity extends AppCompatActivity implements DeleteProductPresenter.View {

    private DeleteProductPresenter presenter;

    @BindView(R.id.cmbProductos) Spinner cmbProductos;
    @BindView(R.id.pb_progress) View viewProgress;
    @BindView(R.id.frmProductDelete) LinearLayout frmProductDelete;
    @BindView(R.id.btnDeleteProducts) Button btnDeleteProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_product);
        ButterKnife.bind(this);

        this.presenter = new DeleteProductPresenter(new DeleteProductInteractor(this));
        this.presenter.setView(this);

        setTitle(getString(R.string.delete_product_title));

        this.presenter.loadProducts();

        btnDeleteProducts.setOnClickListener((View v) -> presenter.deleteProduct());
    }

    @Override
    public void showProgress(boolean show) {
        this.presenter.showProgress(show, frmProductDelete, viewProgress);
    }

    @Override
    public void fillProductList(ArrayList<Product> datos) {
        this.presenter.fillProductList(cmbProductos, datos);
    }

    @Override
    public AppCompatActivity context() {
        return DeleteProductActivity.this;
    }
}
