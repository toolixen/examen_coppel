package me.gildardoibarra.examencoppel.view.activity;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.gildardoibarra.examencoppel.R;
import me.gildardoibarra.examencoppel.presenter.MainPresenter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements MainPresenter.View {

    MainPresenter presenter;

    @BindView(R.id.btnListProducts) Button btnListProducts;
    @BindView(R.id.btnAddProducts) Button btnAddProducts;
    @BindView(R.id.btnDeleteProducts) Button btnDeleteProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = new MainPresenter();
        presenter.setView(this);

        setTitle(getString(R.string.app_name));

        btnListProducts.setOnClickListener((View v) -> presenter.goToActivity(Navigation.ACTIVITY_LIST) );
        btnAddProducts.setOnClickListener((View v) -> presenter.goToActivity(Navigation.ACTIVITY_ADD) );
        btnDeleteProducts.setOnClickListener((View v) -> presenter.goToActivity(Navigation.ACTIVITY_DELETE) );
    }

    @Override
    public AppCompatActivity context() {
        return MainActivity.this;
    }
}
