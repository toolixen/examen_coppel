package me.gildardoibarra.examencoppel.view.activity;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import me.gildardoibarra.examencoppel.R;
import me.gildardoibarra.examencoppel.data.model.Product;
import me.gildardoibarra.examencoppel.databinding.ActivityProductBinding;

public class ProductActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        ActivityProductBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_product);
        Product product = new Product("Producto reciente", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit");
        binding.setProduct(product);
    }
}
