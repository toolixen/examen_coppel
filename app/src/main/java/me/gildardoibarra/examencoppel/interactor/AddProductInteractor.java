package me.gildardoibarra.examencoppel.interactor;

import java.util.ArrayList;

import io.reactivex.functions.Function;
import me.gildardoibarra.examencoppel.ExamenCoppelApplication;
import me.gildardoibarra.examencoppel.data.retrofit.ApiRetrofitCallBack;
import me.gildardoibarra.examencoppel.view.activity.AddProductActivity;
import okhttp3.MultipartBody;

/**
 * Created by anzendigital on 16/04/18.
 */

public class AddProductInteractor {

    private AddProductActivity context;

    public AddProductInteractor(AddProductActivity context) {
        this.context = context;
    }

    public void consumeEndpointAddProducts(String name, String description, Function<ApiRetrofitCallBack.ApiResponse,Boolean> okResponse, Function<ApiRetrofitCallBack.ApiResponse,Boolean> errorResponse) {
        ArrayList<Function<ApiRetrofitCallBack.ApiResponse, Boolean>> functions = new ArrayList<>();
        functions.add((okResponse));
        functions.add(errorResponse);
        ((ExamenCoppelApplication) context.getApplication())
                .getApiClient().addProduct(name, description, functions);
    }

    public void consumeEndpointUploadImage(String id, MultipartBody.Part body, Function<ApiRetrofitCallBack.ApiResponse,Boolean> okResponse, Function<ApiRetrofitCallBack.ApiResponse,Boolean> errorResponse) {
        ArrayList<Function<ApiRetrofitCallBack.ApiResponse, Boolean>> functions = new ArrayList<>();
        functions.add((okResponse));
        functions.add(errorResponse);
        ((ExamenCoppelApplication) context.getApplication())
                .getApiClient().uploadImageProduct(id, body, functions);
    }

}
