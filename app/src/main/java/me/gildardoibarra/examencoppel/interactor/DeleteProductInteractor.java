package me.gildardoibarra.examencoppel.interactor;

import java.util.ArrayList;

import io.reactivex.functions.Function;
import me.gildardoibarra.examencoppel.ExamenCoppelApplication;
import me.gildardoibarra.examencoppel.data.retrofit.ApiRetrofitCallBack;
import me.gildardoibarra.examencoppel.view.activity.DeleteProductActivity;

/**
 * Created by anzendigital on 15/04/18.
 */

public class DeleteProductInteractor {

    private DeleteProductActivity context;

    public DeleteProductInteractor(DeleteProductActivity context) {
        this.context = context;
    }

    public void consumeEndpointListProducts(Function<ApiRetrofitCallBack.ApiResponse,Boolean> okResponse, Function<ApiRetrofitCallBack.ApiResponse,Boolean> errorResponse) {
        ArrayList<Function<ApiRetrofitCallBack.ApiResponse, Boolean>> functions = new ArrayList<>();
        functions.add((okResponse));
        functions.add(errorResponse);
        ((ExamenCoppelApplication) context.getApplication())
                .getApiClient().listProducts(functions);
    }

    public void consumeEndpointDeleteProduct(int id, Function<ApiRetrofitCallBack.ApiResponse,Boolean> okResponse, Function<ApiRetrofitCallBack.ApiResponse,Boolean> errorResponse) {
        ArrayList<Function<ApiRetrofitCallBack.ApiResponse, Boolean>> functions = new ArrayList<>();
        functions.add((okResponse));
        functions.add(errorResponse);
        ((ExamenCoppelApplication) context.getApplication())
                .getApiClient().deleteProduct(id, functions);
    }

}
