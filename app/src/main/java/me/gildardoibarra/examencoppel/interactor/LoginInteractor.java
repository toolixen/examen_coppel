package me.gildardoibarra.examencoppel.interactor;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import io.reactivex.functions.Function;
import me.gildardoibarra.examencoppel.ExamenCoppelApplication;
import me.gildardoibarra.examencoppel.data.client.ApiClient;
import me.gildardoibarra.examencoppel.data.retrofit.ApiRetrofitCallBack;
import me.gildardoibarra.examencoppel.presenter.LoginPresenter;
import me.gildardoibarra.examencoppel.view.activity.LoginActivity;

/**
 * Created by anzendigital on 13/04/18.
 */

public class LoginInteractor {

    private LoginActivity context;

    public LoginInteractor(LoginActivity context) {
        this.context = context;
    }

    public void consumeEndpointLogin(String username, String password, Function<ApiRetrofitCallBack.ApiResponse,Boolean> okResponse, Function<ApiRetrofitCallBack.ApiResponse,Boolean> errorResponse) {
        ArrayList<Function<ApiRetrofitCallBack.ApiResponse, Boolean>> functions = new ArrayList<>();
        functions.add((okResponse));
        functions.add(errorResponse);
        ((ExamenCoppelApplication) context.getApplication())
                .getApiClient().login(username, password, functions);
    }

}
