package me.gildardoibarra.examencoppel.presenter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by anzendigital on 13/04/18.
 */

public abstract class Presenter<T extends Presenter.View> {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private T view;

    public T getView() {
        return view;
    }

    public void setView(T view) {
        this.view = view;
    }

    public void initialize() {

    }

    public void terminate() {
        dispose();
    }

    void addDisposableObserver(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    private void dispose() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public interface View {
        AppCompatActivity context();
    }
}