package me.gildardoibarra.examencoppel.presenter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import java.util.ArrayList;
import me.gildardoibarra.examencoppel.data.model.Product;
import me.gildardoibarra.examencoppel.data.retrofit.ApiRetrofitCallBack;
import me.gildardoibarra.examencoppel.interactor.DeleteProductInteractor;
import me.gildardoibarra.examencoppel.view.activity.LoginActivity;

/**
 * Created by anzendigital on 15/04/18.
 */

public class DeleteProductPresenter extends Presenter<DeleteProductPresenter.View> {

    private DeleteProductInteractor interactor;
    private Product productSelected;

    public DeleteProductPresenter(DeleteProductInteractor interactor) {
        this.interactor = interactor;
        this.productSelected = null;
    }

    public void loadProducts() {
        getView().showProgress(true);
        this.interactor.consumeEndpointListProducts((ApiRetrofitCallBack.ApiResponse response) -> {
            if ( response.code != 200 ) return false;
            getView().fillProductList((ArrayList<Product>) response.data);
            return true;
        }, (ApiRetrofitCallBack.ApiResponse response) -> {
            if ( response.code != 401 ) return false;
            new AlertDialog.Builder(getView().context())
                    .setTitle("Sesion expirada")
                    .setMessage("Necesita volver a iniciar sesion.")
                    .setPositiveButton("Ok", (DialogInterface dialog, int which) -> {
                        Intent login = new Intent(getView().context().getApplicationContext(), LoginActivity.class);
                        getView().context().startActivity(login);
                        getView().context().finish();
                    })
                    .setCancelable(false)
                    .create()
                    .show();

            return true;
        });
    }

    public void fillProductList(Spinner cmbProductos, ArrayList<Product> datos) {

        final Product[] products = new Product[datos.size()];
        for ( int i = 0 ; i < datos.size() ; i++ ) products[i] = datos.get(i);

        ArrayAdapter<Product> adaptador = new ArrayAdapter(getView().context(), android.R.layout.simple_spinner_item, products);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cmbProductos.setAdapter(adaptador);
        cmbProductos.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> parent, android.view.View view, int position, long id) {
                productSelected = (Product) parent.getItemAtPosition(position);
            }
            @Override public void onNothingSelected(AdapterView<?> parent) {}
        });
        getView().showProgress(false);
    }

    public void showProgress(final boolean show, android.view.View viewLoginForm, android.view.View viewProgress) {

        int shortAnimTime = getView().context().getResources().getInteger(android.R.integer.config_shortAnimTime);

        viewLoginForm.setVisibility(show ? android.view.View.GONE : android.view.View.VISIBLE);
        viewLoginForm.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                viewLoginForm.setVisibility(show ? android.view.View.GONE : android.view.View.VISIBLE);
            }
        });

        viewProgress.setVisibility(show ? android.view.View.VISIBLE : android.view.View.GONE);
        viewProgress.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                viewProgress.setVisibility(show ? android.view.View.VISIBLE : android.view.View.GONE);
            }
        });

    }

    public void deleteProduct() {
        if ( productSelected != null ) {
            this.interactor.consumeEndpointDeleteProduct(productSelected.getId(), (ApiRetrofitCallBack.ApiResponse response) -> {
                if ( response.code != 200 ) return false;
                new AlertDialog.Builder(getView().context())
                        .setTitle("Producto eliminado")
                        .setMessage("Se elimino correctamente el producto: " + productSelected.getName())
                        .setPositiveButton("Aceptar", (DialogInterface dialog, int which) -> {
                            getView().context().finish();
                        })
                        .setCancelable(false)
                        .create()
                        .show();
                return true;
            }, (ApiRetrofitCallBack.ApiResponse response) -> {
                if ( response.code != 401 ) return false;
                new AlertDialog.Builder(getView().context())
                        .setTitle("Sesion expirada")
                        .setMessage("Necesita volver a iniciar sesion.")
                        .setPositiveButton("Aceptar", (DialogInterface dialog, int which) -> {
                            Intent login = new Intent(getView().context().getApplicationContext(), LoginActivity.class);
                            getView().context().startActivity(login);
                            getView().context().finish();
                        })
                        .setCancelable(false)
                        .create()
                        .show();
                return true;
            });
        }
    }

    public interface View extends Presenter.View {
        void showProgress(boolean show);
        void fillProductList(ArrayList<Product> datos);
    }
}
