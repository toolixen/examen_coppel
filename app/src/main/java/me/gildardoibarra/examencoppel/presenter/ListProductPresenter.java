package me.gildardoibarra.examencoppel.presenter;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import java.util.ArrayList;
import me.gildardoibarra.examencoppel.data.model.Product;
import me.gildardoibarra.examencoppel.data.retrofit.ApiRetrofitCallBack;
import me.gildardoibarra.examencoppel.interactor.ListProductInteractor;
import me.gildardoibarra.examencoppel.view.activity.LoginActivity;
import me.gildardoibarra.examencoppel.view.adapter.ProductAdapter;

/**
 * Created by anzendigital on 14/04/18.
 */

public class ListProductPresenter extends Presenter<ListProductPresenter.View> {

    private ListProductInteractor interactor;

    public ListProductPresenter(ListProductInteractor interactor) {
        this.interactor = interactor;
    }

    public void showProgress(final boolean show, android.view.View viewLoginForm, android.view.View viewProgress) {

        int shortAnimTime = getView().context().getResources().getInteger(android.R.integer.config_shortAnimTime);

        viewLoginForm.setVisibility(show ? android.view.View.GONE : android.view.View.VISIBLE);
        viewLoginForm.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                viewLoginForm.setVisibility(show ? android.view.View.GONE : android.view.View.VISIBLE);
            }
        });

        viewProgress.setVisibility(show ? android.view.View.VISIBLE : android.view.View.GONE);
        viewProgress.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                viewProgress.setVisibility(show ? android.view.View.VISIBLE : android.view.View.GONE);
            }
        });

    }

    public void loadProducts() {
        getView().showProgress(true);
        this.interactor.consumeEndpointListProducts((ApiRetrofitCallBack.ApiResponse response) -> {
            if ( response.code != 200 ) return false;
            getView().fillProductList((ArrayList<Product>) response.data);
            return true;
        }, (ApiRetrofitCallBack.ApiResponse response) -> {
            if ( response.code != 401 ) return false;
            new AlertDialog.Builder(getView().context())
                    .setTitle("Sesion expirada")
                    .setMessage("Necesita volver a iniciar sesion.")
                    .setPositiveButton("Ok", (DialogInterface dialog, int which) -> {
                        Intent login = new Intent(getView().context().getApplicationContext(), LoginActivity.class);
                        getView().context().startActivity(login);
                        getView().context().finish();
                    })
                    .setCancelable(false)
                    .create()
                    .show();

            return true;
        });
    }

    public void fillProductList(RecyclerView lstProduct, ArrayList<Product> datos){

        lstProduct.setHasFixedSize(true);

        final ProductAdapter adaptador = new ProductAdapter(datos);

        adaptador.setOnClickListener((android.view.View v) -> {
            Log.i("DemoRecView", "Pulsado el elemento " + lstProduct.getChildAdapterPosition(v));
        });

        lstProduct.setAdapter(adaptador);
        lstProduct.setLayoutManager(new LinearLayoutManager(getView().context(), LinearLayoutManager.VERTICAL,false));

        getView().showProgress(false);
    }

    public interface View extends Presenter.View {
        void showProgress(boolean show);
        void fillProductList(ArrayList<Product> datos);
    }
}
