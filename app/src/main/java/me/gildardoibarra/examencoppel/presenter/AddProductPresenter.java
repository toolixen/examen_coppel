package me.gildardoibarra.examencoppel.presenter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import io.reactivex.functions.Function;
import me.gildardoibarra.examencoppel.data.model.Product;
import me.gildardoibarra.examencoppel.data.retrofit.ApiRetrofitCallBack;
import me.gildardoibarra.examencoppel.interactor.AddProductInteractor;
import me.gildardoibarra.examencoppel.view.activity.LoginActivity;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by anzendigital on 16/04/18.
 */

public class AddProductPresenter extends Presenter<AddProductPresenter.View> {

    private AddProductInteractor interactor;
    private Product product;
    private Function<ApiRetrofitCallBack.ApiResponse,Boolean> notAuthenticated = (ApiRetrofitCallBack.ApiResponse response) -> {
        if ( response.code != 401 ) return false;
        new AlertDialog.Builder(getView().context())
                .setTitle("Sesion expirada")
                .setMessage("Necesita volver a iniciar sesion.")
                .setPositiveButton("Ok", (DialogInterface dialog, int which) -> {
                    Intent login = new Intent(getView().context(), LoginActivity.class);
                    getView().context().startActivity(login);
                    getView().context().finish();
                })
                .setCancelable(false)
                .create()
                .show();

        return true;
    };

    public AddProductPresenter(AddProductInteractor interactor) {
        this.interactor = interactor;
        this.product = null;
    }

    public void uploadImage(Bitmap imageBitmap, String id) {

        File f = new File(getView().context().getCacheDir(), "product_image");
        try {
            f.createNewFile();
        } catch (IOException e) {
            Log.e("Error bitmap", e.getMessage());
            e.printStackTrace();
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.PNG, 10 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            Log.e("Error FileNotFound", e.getMessage());
            e.printStackTrace();
        }

        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), f);
        MultipartBody.Part body = MultipartBody.Part.createFormData("upload", f.getName(), reqFile);

        this.interactor.consumeEndpointUploadImage(id, body, (ApiRetrofitCallBack.ApiResponse response) -> {
            if ( response.code != 201 ) return false;
            new AlertDialog.Builder(getView().context())
                    .setTitle("Producto agregado")
                    .setMessage("Se agrego correctamente el producto: " + this.product.getName())
                    .setPositiveButton("Aceptar", (DialogInterface dialog, int which) -> {
                        getView().context().finish();
                    })
                    .setCancelable(false)
                    .create()
                    .show();
            return true;
        }, notAuthenticated);
    }

    public void addProduct(String name, String description) {
        getView().showProgress(true);
        this.interactor.consumeEndpointAddProducts(name, description, (ApiRetrofitCallBack.ApiResponse response) -> {
            if ( response.code != 201 ) return false;
            this.product = (Product) response.data;
            getView().uploadImage( String.valueOf(product.getId()) );
            return true;
        }, notAuthenticated);
    }

    public void showProgress(final boolean show, android.view.View viewLoginForm, android.view.View viewProgress) {

        int shortAnimTime = getView().context().getResources().getInteger(android.R.integer.config_shortAnimTime);

        viewLoginForm.setVisibility(show ? android.view.View.GONE : android.view.View.VISIBLE);
        viewLoginForm.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                viewLoginForm.setVisibility(show ? android.view.View.GONE : android.view.View.VISIBLE);
            }
        });

        viewProgress.setVisibility(show ? android.view.View.VISIBLE : android.view.View.GONE);
        viewProgress.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                viewProgress.setVisibility(show ? android.view.View.VISIBLE : android.view.View.GONE);
            }
        });

    }

    public interface View extends Presenter.View {
        void showProgress(boolean show);
        void uploadImage(String id);
    }
}
