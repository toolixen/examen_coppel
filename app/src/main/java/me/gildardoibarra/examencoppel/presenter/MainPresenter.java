package me.gildardoibarra.examencoppel.presenter;

import android.content.Intent;
import me.gildardoibarra.examencoppel.view.activity.AddProductActivity;
import me.gildardoibarra.examencoppel.view.activity.DeleteProductActivity;
import me.gildardoibarra.examencoppel.view.activity.ListProductActivity;

/**
 * Created by anzendigital on 13/04/18.
 */

public class MainPresenter extends Presenter<MainPresenter.View> {

    public void goToActivity(int activity) {
        Intent nextActivity = null;
        switch (activity) {
            case View.Navigation.ACTIVITY_LIST:
                nextActivity = new Intent(getView().context().getApplicationContext(), ListProductActivity.class);
                break;
            case View.Navigation.ACTIVITY_ADD:
                nextActivity = new Intent(getView().context().getApplicationContext(), AddProductActivity.class);
                break;
            case View.Navigation.ACTIVITY_DELETE:
                nextActivity = new Intent(getView().context().getApplicationContext(), DeleteProductActivity.class);
                break;
        }
        getView().context().startActivity(nextActivity);
    }

    public interface View extends Presenter.View {
        interface Navigation {
            int ACTIVITY_LIST = 1;
            int ACTIVITY_ADD = 2;
            int ACTIVITY_DELETE = 3;
        }
    }

}
