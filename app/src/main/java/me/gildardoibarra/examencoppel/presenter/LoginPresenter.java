package me.gildardoibarra.examencoppel.presenter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import me.gildardoibarra.examencoppel.ExamenCoppelApplication;
import me.gildardoibarra.examencoppel.R;
import me.gildardoibarra.examencoppel.data.model.LoginData;
import me.gildardoibarra.examencoppel.data.retrofit.ApiRetrofitCallBack;
import me.gildardoibarra.examencoppel.interactor.LoginInteractor;
import me.gildardoibarra.examencoppel.view.activity.MainActivity;

/**
 * Created by anzendigital on 13/04/18.
 */

public class LoginPresenter extends Presenter<LoginPresenter.View> {

    private LoginInteractor interactor;

    public LoginPresenter(LoginInteractor interactor) {
        this.interactor = interactor;
    }

    public void submitLogin(EditText txtUsername, EditText txtPassword) {
        txtUsername.setError(null);
        txtPassword.setError(null);

        String username = txtUsername.getText().toString();
        String password = txtPassword.getText().toString();

        boolean cancel = false;
        android.view.View focusView = null;

        if ( TextUtils.isEmpty(username) ) {
            txtUsername.setError(getView().context().getString(R.string.error_field_required));
            focusView = txtUsername;
            cancel = true;
        }

        if ( TextUtils.isEmpty(password) ) {
            txtPassword.setError(getView().context().getString(R.string.error_field_required));
            focusView = txtPassword;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            getView().showProgress(true);
            consumeEndpointLogin(username, password);
        }
    }

    private void consumeEndpointLogin(String username, String password) {
        interactor.consumeEndpointLogin(username, password, (ApiRetrofitCallBack.ApiResponse response) -> {
            if ( response.code != 200 ) return false;

            Log.e("Endpoint", ((LoginData) response.data).getToken() );

            ( (ExamenCoppelApplication) getView().context().getApplication() )
                    .getApiClient().setAuthHeader( ( (LoginData) response.data ).getToken() );

            Intent navegacion = new Intent(getView().context().getApplicationContext(), MainActivity.class);
            getView().context().startActivity(navegacion);
            getView().context().finish();
            return true;
        }, (ApiRetrofitCallBack.ApiResponse response) -> {
            if ( response.code != 401 ) return false;
            getView().showProgress(false);
            new AlertDialog.Builder(getView().context())
                    .setMessage("Inicio de sesion incorrecto")
                    .setTitle("Error")
                    .setPositiveButton("Aceptar", (DialogInterface dialog, int which) -> {})
                    .create()
                    .show();

            return true;
        });
    }

    public void showProgress(final boolean show, android.view.View viewLoginForm, android.view.View viewProgress) {

        int shortAnimTime = getView().context().getResources().getInteger(android.R.integer.config_shortAnimTime);

        viewLoginForm.setVisibility(show ? android.view.View.GONE : android.view.View.VISIBLE);
        viewLoginForm.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                viewLoginForm.setVisibility(show ? android.view.View.GONE : android.view.View.VISIBLE);
            }
        });

        viewProgress.setVisibility(show ? android.view.View.VISIBLE : android.view.View.GONE);
        viewProgress.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                viewProgress.setVisibility(show ? android.view.View.VISIBLE : android.view.View.GONE);
            }
        });

    }

    public interface View extends Presenter.View {
        void submitLogin();
        void showProgress(boolean show);
        FragmentManager getTheFragmentManager();
    }
}
