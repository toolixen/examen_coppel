package me.gildardoibarra.examencoppel.data.client;

import android.util.Log;
import java.util.ArrayList;
import javax.inject.Inject;
import io.reactivex.functions.Function;
import me.gildardoibarra.examencoppel.data.retrofit.ApiRetrofitCallBack;
import me.gildardoibarra.examencoppel.data.retrofit.ApiRetrofitClient;
import okhttp3.MultipartBody;

/**
 * Created by anzendigital on 13/04/18.
 */

public class ApiClient extends ApiRetrofitClient {

    @Inject
    public ApiClient() {}

    public void login(String username, String pass, ArrayList<Function<ApiRetrofitCallBack.ApiResponse,Boolean>> functionResponses) {
        restClient
                .login(username, pass)
                .enqueue(new ApiRetrofitCallBack(functionResponses));
    }

    public void listProducts(ArrayList<Function<ApiRetrofitCallBack.ApiResponse,Boolean>> functionResponses) {
        Log.e("Token", this.authHeader);
        restClient
                .listProducts("Bearer "+this.authHeader)
                .enqueue(new ApiRetrofitCallBack(functionResponses));
    }

    public void deleteProduct(int id, ArrayList<Function<ApiRetrofitCallBack.ApiResponse,Boolean>> functionResponses) {
        restClient
                .deleteProduct("Bearer "+this.authHeader, String.valueOf(id))
                .enqueue(new ApiRetrofitCallBack(functionResponses));
    }

    public void addProduct(String name, String description, ArrayList<Function<ApiRetrofitCallBack.ApiResponse,Boolean>> functionResponses) {
        restClient
                .addProduct("Bearer "+this.authHeader, name, description)
                .enqueue(new ApiRetrofitCallBack(functionResponses));
    }

    public void uploadImageProduct(String id, MultipartBody.Part body, ArrayList<Function<ApiRetrofitCallBack.ApiResponse,Boolean>> functionResponses) {
        restClient
                .uploadImageProduct("Bearer "+this.authHeader, id, body )
                .enqueue(new ApiRetrofitCallBack(functionResponses));
    }

}