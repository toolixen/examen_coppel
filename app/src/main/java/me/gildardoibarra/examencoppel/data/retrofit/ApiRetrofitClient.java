package me.gildardoibarra.examencoppel.data.retrofit;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by anzendigital on 12/04/18.
 */

public class ApiRetrofitClient {

    protected ApiRetrofitService restClient;
    protected Gson gson;
    protected String authHeader;

    public ApiRetrofitClient() {

        gson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create();

        Retrofit retrofit = new Retrofit.Builder()
            .client(
                    new OkHttpClient.Builder()
                            .connectTimeout(100, TimeUnit.SECONDS)
                            .readTimeout(100,TimeUnit.SECONDS).build()
            )
            .baseUrl( Constants.BASE_URL )
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

        restClient = retrofit.create(ApiRetrofitService.class);
    }

    public String getAuthHeader() {
        return authHeader;
    }

    public void setAuthHeader(String authHeader) {
        this.authHeader = authHeader;
    }

}
