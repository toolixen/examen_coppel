package me.gildardoibarra.examencoppel.data.retrofit;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.functions.Function;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anzendigital on 12/04/18.
 */

public class ApiRetrofitCallBack<T> implements Callback<T> {

    private ArrayList<Function<ApiResponse,Boolean>> functionResponses;

    public ApiRetrofitCallBack(ArrayList<Function<ApiResponse,Boolean>> functionResponses) {
        this.functionResponses = functionResponses;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        Log.e("Respuesta Retrofit", "Code: "+response.code());
        for (Function function : functionResponses) {
            try {
                function.apply( new ApiResponse(response.code(),response.body()) );
            } catch (Exception e) {
                Log.e("ApiCallBack", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Log.e("Error in ApiCallBack", t.getMessage());
        t.printStackTrace();
    }

    public class ApiResponse {
        public int code;
        public T data;

        public ApiResponse(int code, T data) {
            this.code = code;
            this.data = data;
        }
    }
}