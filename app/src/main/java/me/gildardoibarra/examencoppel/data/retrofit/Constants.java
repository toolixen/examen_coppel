package me.gildardoibarra.examencoppel.data.retrofit;

/**
 * Created by anzendigital on 12/04/18.
 */

public class Constants {

    public static final String BASE_URL = "http://159.65.102.203/ex_coppel/api/";
    public static final String BASE_URL_IMAGES = "http://159.65.102.203/ex_coppel/api/uploadedimages/";

    public static final class Endpoint {
        public static final String LOGIN = "login";
        public static final String PRODUCTS = "products";
        public static final String DELETE_PRODUCT = "products/{id}";
        public static final String IMAGE_PRODUCT = "products/{id}/image";
    }

    public static final class Params {
        public static final String HEADER_AUTHORIZATION = "Authorization";
        public static final String USERNAME = "username";
        public static final String PASSWORD = "pass";
        public static final String PRODUCT_ID = "id";
        public static final String PRODUCT_NAME = "name";
        public static final String PRODUCT_DESCRIPTION = "description";
    }

}
