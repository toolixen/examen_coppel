package me.gildardoibarra.examencoppel.data.retrofit;


import java.util.ArrayList;
import me.gildardoibarra.examencoppel.data.model.LoginData;
import me.gildardoibarra.examencoppel.data.model.Product;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by anzendigital on 12/04/18.
 */

public interface ApiRetrofitService {

    @FormUrlEncoded
    @POST(Constants.Endpoint.LOGIN) Call<LoginData> login(
        @Field(Constants.Params.USERNAME) String username,
        @Field(Constants.Params.PASSWORD) String pass
    );

    @GET(Constants.Endpoint.PRODUCTS)
    Call<ArrayList<Product>> listProducts(
            @Header(Constants.Params.HEADER_AUTHORIZATION) String authHeader
    );

    @DELETE(Constants.Endpoint.DELETE_PRODUCT)
    Call<ArrayList<Product>> deleteProduct(
            @Header(Constants.Params.HEADER_AUTHORIZATION) String authHeader,
            @Path(Constants.Params.PRODUCT_ID) String id
    );

    @FormUrlEncoded
    @POST(Constants.Endpoint.PRODUCTS) Call<Product> addProduct(
            @Header(Constants.Params.HEADER_AUTHORIZATION) String authHeader,
            @Field(Constants.Params.PRODUCT_NAME) String name,
            @Field(Constants.Params.PRODUCT_DESCRIPTION) String description
    );

    @Multipart
    @POST(Constants.Endpoint.IMAGE_PRODUCT)
    Call<ArrayList> uploadImageProduct(
            @Header(Constants.Params.HEADER_AUTHORIZATION) String authHeader,
            @Path(Constants.Params.PRODUCT_ID) String id,
            @Part MultipartBody.Part image
    );

}
