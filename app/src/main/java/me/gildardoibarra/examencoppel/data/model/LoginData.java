package me.gildardoibarra.examencoppel.data.model;

/**
 * Created by anzendigital on 12/04/18.
 */

public class LoginData {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
