package me.gildardoibarra.examencoppel;

import android.app.Application;

import me.gildardoibarra.examencoppel.data.client.ApiClient;

/**
 * Created by anzendigital on 15/04/18.
 */

public class ExamenCoppelApplication extends Application {

    ApiClient apiClient;

    @Override
    public void onCreate() {
        super.onCreate();

        apiClient = new ApiClient();
    }

    public ApiClient getApiClient() {
        return apiClient;
    }
}
